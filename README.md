# README #

Package Description

## This package implements the following methodology for data with paired samples ##

* modified t-statistic of Kim
* corrected Z-test of Looney and Jones
* MLE based test of Ekbohm under homoscedasticity
* MLE based test of Lin and Stivers under heteroscedasticity
* weighted Z-test combination